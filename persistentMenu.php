<?php
$data = [
    'persistent_menu' => [
        [
            'locale'                  => 'default',
            'composer_input_disabled' => false,
            ""
            'call_to_actions'         => [
                [
                    'title'           => 'Frequently Asked Questions',
                    'type'            => 'nested',
                    'call_to_actions' => [
                        [
                            'title'   => 'How much are tickets?',
                            'type'    => 'postback',
                            'payload' => 'price',
                        ],
                        [
                            'title'   => 'How long can I expect to wait?',
                            'type'    => 'postback',
                            'payload' => 'wait',
                        ],
                        [
                            'title'   => 'I would like to contact you...',
                            'type'    => 'postback',
                            'payload' => 'contact',
                        ],
                    ],
                ],
                [
                    'title'           => 'Cableway Status and Weather',
                    'type'            => 'nested',
                    'call_to_actions' => [
                        [
                            'title'   => 'Is the cableway open?',
                            'type'    => 'postback',
                            'payload' => 'open',
                        ],
                        [
                            'title'   => 'First and last cars?',
                            'type'    => 'postback',
                            'payload' => 'hours',
                        ],
                        [
                            'title'   => 'How is the weather?',
                            'type'    => 'postback',
                            'payload' => 'weather',
                        ],
                        [
                            'title'   => 'How is the visibility?',
                            'type'    => 'postback',
                            'payload' => 'vis',
                        ],
                    ],
                ],
                [
                    'type'                 => 'web_url',
                    'title'                => 'Tickets',
                    'url'                  => 'http://www.webtickets.co.za/event.aspx?itemid=681080',
                    'webview_height_ratio' => 'full',
                ],
            ],
        ],
    ],
];


$URL = "https://graph.facebook.com/v2.9/me/messenger_profile?access_token=EAAECMrGljWIBAOnaR5HRxABScEDBpM1NwgzkZCNpaTXXsYBXw6AjGZAR6O55OzJAmqyLzYZAB3zmHqarCL7GdJNSnx7pVGYi91zA9yMCsBYnaVi8D4SEpZBJ5bpM7Pt9onayo4oEZB89pz8CWsBXV5MFWMSOiZAgFhTrKxmD6o3wZDZD";

$ch = curl_init($URL);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
$result = curl_exec($ch);
