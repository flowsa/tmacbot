<?php
/* need to make into array and then json encode, like persistenmenu.php
curl -X POST -H "Content-Type: application/json" -d '{
  "greeting":[
    {
      "locale":"default",
      "text":"Welcome to Table Mountain Aerial Cableway. Take the fast way up!"
    }, {
      "locale":"en_US",
      "text":"Welcome to Table Mountain Aerial Cableway. Take the fast way up!"
    }
  ]
}'
*/
$URL = "https://graph.facebook.com/v2.9/me/messenger_profile?access_token=EAAECMrGljWIBAOnaR5HRxABScEDBpM1NwgzkZCNpaTXXsYBXw6AjGZAR6O55OzJAmqyLzYZAB3zmHqarCL7GdJNSnx7pVGYi91zA9yMCsBYnaVi8D4SEpZBJ5bpM7Pt9onayo4oEZB89pz8CWsBXV5MFWMSOiZAgFhTrKxmD6o3wZDZD";

$ch = curl_init($URL);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
$result = curl_exec($ch);

curl -X POST -H "Content-Type: application/json" -d '{
  "recipient":{
    "id":"USER_ID"
  },
  "message":{
    "text":"Pick a color:",
    "quick_replies":[
      {
        "content_type":"text",
        "title":"Red",
        "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
      },
      {
        "content_type":"text",
        "title":"Green",
        "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_GREEN"
      }
    ]
  }
}' https://graph.facebook.com/v2.9/me/messenger_profile?access_token=EAAECMrGljWIBAOnaR5HRxABScEDBpM1NwgzkZCNpaTXXsYBXw6AjGZAR6O55OzJAmqyLzYZAB3zmHqarCL7GdJNSnx7pVGYi91zA9yMCsBYnaVi8D4SEpZBJ5bpM7Pt9onayo4oEZB89pz8CWsBXV5MFWMSOiZAgFhTrKxmD6o3wZDZD
curl -X DELETE -H "Content-Type: application/json" -d '{
  "fields":[
    "persistent_menu"
  ]
}' https://graph.facebook.com/v2.9/me/messenger_profile?access_token=EAAECMrGljWIBAOnaR5HRxABScEDBpM1NwgzkZCNpaTXXsYBXw6AjGZAR6O55OzJAmqyLzYZAB3zmHqarCL7GdJNSnx7pVGYi91zA9yMCsBYnaVi8D4SEpZBJ5bpM7Pt9onayo4oEZB89pz8CWsBXV5MFWMSOiZAgFhTrKxmD6o3wZDZD
