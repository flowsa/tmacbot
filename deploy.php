<?php
namespace Deployer;
require 'recipe/common.php';

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/* Dependencies

This set up file is passwordless - in order to achieve this:

1. You must have ~/.my.cnf configured with mysql root user both locally and remote (deploy user)
2. You must have your public key installed on /home/deploy/.ssh/authorized_keys

*/

// Configuration

$project_name = "tmacbot";
$production_server = 'flowroot7.flowsa.net';
$production_user = 'deploy';
$db_name = $project_name . '_live';
$symlinkedFolders = [
  "public/uploads" => "uploads"
];

// $project_type = "craftcms" // options: craftcms, ee1, ee2, laravel, codeigniter, cakephp

// ---------------------------------------------------
// YOU SHOULDN'T NEED TO EDIT ANYTHING BELOW THIS LINE
// THE LOGIC BELOW MAY BE 'UPGRADED' AT ANY TIME
// ---------------------------------------------------

set('ssh_type', 'native');
set('ssh_multiplexing', true);

option('full', false, InputOption::VALUE_OPTIONAL, 'Full database import option');

set('project_name', $project_name);
set('production_server', $production_server);
set('production_user', $production_user);

// Deploy path
set('project_path', '/var/www/' . $project_name);

// Repo
set('repository', 'git@bitbucket.org:flowsa/'.$project_name.'.git');

// Production database name
set('db_name', $db_name);

// Shared dirs usually left blank as we do our own symlinking
add('shared_files', []);
add('shared_dirs', []);

set('default_stage', 'staging');

server('staging', $production_server)
    ->user($production_user)
    ->identityFile()
    ->set('deploy_path', '/var/www/'.$project_name.'/test')
    ->set('branch', 'develop')
    ->set('env', 'test')
    ->stage('staging');

server('production', $production_server)
    ->user($production_user)
    ->identityFile()
    ->set('deploy_path', '/var/www/'.$project_name.'/live')
    ->set('branch', 'master')
    ->set('env', 'live')
    ->stage('production');


task('deploy:static_folders', function () use ($symlinkedFolders) {

    foreach ($symlinkedFolders as $dest => $source) {
      staticSymlink($dest, $source);
    }

});

//
// DO NOT EDIT BELOW UNLESS YOU KNOW WHAT YOU'RE DOING
//

task('migrate:import_db', function () {

    $db_name = get('db_name');

    // Ignore session tables - currently only EE example but can add to this
    if (input()->getOption('full')) {
      $ignore_sessions_table = "";
    } else {
      $ignore_sessions_table = "--ignore-table=$db_name.exp_sessions";
    }

    desc('Import database');

    $timeout = 600;
    $date = date('dMY.Hi');
    $remoteDumpsPath = "{{project_path}}/db_dumps";
    $remoteDbDump = "$db_name.$date.remote.sql";
    $remoteDbDumpGz = $remoteDbDump . ".gz";

    $localDumpsPath = "{{project_path}}/db_dumps";
    $localDbDump = "$db_name.$date.local.sql";
    $localDbDumpGz = $localDbDump . ".gz";

    writeln('Creating dir for local databases ');

    runLocally("mkdir -p {{project_path}}/db_dumps", $timeout);

    writeln('Making remote dir {{project_path}}/db_dumps');

    run("mkdir -p $remoteDumpsPath");

    writeln('gzip remote DB');

    run("cd $remoteDumpsPath && mysqldump $db_name $ignore_sessions_table | gzip > $remoteDumpsPath/$remoteDbDumpGz");

    writeln('Copy remote DB locally');

    download("../db_dumps/$remoteDbDumpGz", "$remoteDumpsPath/$remoteDbDumpGz");

    writeln("Create local database if it doesn't already exist");

    runLocally("mysql -e 'create database IF NOT EXISTS " . $db_name . "'", $timeout);

    writeln("Extracting remote DB");
    runLocally("gunzip -f ../db_dumps/$remoteDbDumpGz");

    writeln("Search replace DB - craftcms specific ");

    $sed_phrase = 's/\\\"protocol\\\":\\\"smtp\\\"/\\\"protocol\\\":\\\"php\\\"/g';
    $sed_command = "sed -i -e '$sed_phrase' ../db_dumps/$remoteDbDump";

    runLocally($sed_command);

    writeln("Import DB");

    runLocally("cat ../db_dumps/$remoteDbDump | mysql $db_name");

    // TODO write clean up scripts
});


task('migrate:export_db', function () {

    $db_name = get('db_name');

    desc('Export database');

    $timeout = 600;
    $date = date('dMYhi');
    $remoteDumpsPath = "{{project_path}}/db_dumps";
    $remoteDbDump = "$db_name.$date.remote.sql";
    $remoteDbDumpGz = $remoteDbDump . ".gz";

    $localDumpsPath = "{{project_path}}/db_dumps";
    $localDbDump = "$db_name.$date.local.sql";
    $localDbDumpGz = $localDbDump . ".gz";

    writeln('Creating dir for local databases ');

    runLocally("mkdir -p {{project_path}}", $timeout);

    writeln('Making remote dir {{project_path}}/db_dumps');

    run("mkdir -p $remoteDumpsPath");

    writeln('gzip local DB');

    runLocally("mysqldump $db_name --ignore-table=$db_name.exp_sessions | gzip > $localDumpsPath/$localDbDumpGz");

    writeln('upload to remote DB');

    upload("../db_dumps/$localDbDumpGz", "$remoteDumpsPath/$localDbDumpGz");

    writeln("Extracting remote DB");
    run("gunzip -f {{project_path}}/db_dumps/$localDbDumpGz");

    writeln("Make backup of current remote DB");

    run("mysqldump $db_name --ignore-table=$db_name.exp_sessions | gzip > $remoteDumpsPath/$remoteDbDumpGz");

    writeln("Import DB");

    $result = askConfirmation("Are you sure you want to overwrite the current database?");

    if ($result) {
      run("cat {{project_path}}/db_dumps/$localDbDump | mysql $db_name");
    }

});


task('migrate:import_files', function () {

  desc('Import files');

  $folders = ['static/uploads' => 'public/uploads'];

  $rsyncSettings = "";

  foreach ($folders as $source => $destination) {

    runLocally("rsync -avP $rsyncSettings {{production_user}}@{{production_server}}:{{project_path}}/$source/. $destination/. ");

    # code...
  }


});


task('local:init', function () {

  desc('Symlink git hooks');

  runLocally("rm -rf .git/hooks && ln -s ../git_hooks .git/hooks");

});


task('deploy:vendors', function () {
  // This task overrides the default vendors task

  desc('Run dependency installation (usually composer)');

  // Assumes docker image has composer
  run("docker exec --user deploy -i  {{project_name}}.{{env}}.web bash -c 'cd {{release_path}}/app && composer install'");

});

task('deploy:envlink', function () {
  desc('Symlink env file');
  run("ln -s {{release_path}}/.env.production {{release_path}}/.env");
});


desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    // 'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:static_folders',
    'deploy:writable',
    // 'deploy:vendors',
    // 'deploy:envlink',
    'deploy:clear_paths',
    'deploy:symlink',
    // 'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// FLOW SPECIFIC FUNCTIONS

function staticSymlink($releaseDir, $staticDir) {
  run("rm -rf {{release_path}}/$releaseDir");
  run("mkdir -p {{project_path}}/static/$staticDir");
  run("{{bin/symlink}} {{project_path}}/static/$staticDir {{release_path}}/$releaseDir");
}
