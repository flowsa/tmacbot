<?php
function setTheMood($postbackPayload,$tmacStat)
{
  switch ($postbackPayload)
  {
    case "open":
      if (strtolower($tmacStat[0]['cableway_status'])!="open")
      {
        return "closed";
      }
      else
      {
        return "open";
      }
      break;
    case "closed":
      if (strtolower($tmacStat[0]['cableway_status'])!="open")
      {
        return "closed";
      }
      else
      {
        return "open";
      }
      break;
    case "weather":
      if (strtolower($tmac[0]['temperature'])=="cold")
      {
        return "cold";
      }
      else
      {
        return "warm";
      }
        break;
    default:
      return $postbackPayload;
  }
}

#determining whether or not "special" hours apply
function dateChecker()
{
  $checkedDate="";
  if (((date('M') == "Dec") && ((int)date('d') > 15)) || ((date('M') == "Jan") && ((int)date('d') < 16)))
  {

    $checkedDate = "16";
    return $checkedDate;
  }
  else
  {
    $checkedDate = date('M');
    return $checkedDate;
  }
}

#ngram a word
function ngramThis($inputMessage)
{
  $characters= [",",".","?"," ","!","-"];
  $word = "";
  $ngrams=[];
  for ($i=0;$i<strlen($inputMessage);++$i)
  {
      if ( !in_array($inputMessage[$i], $characters))
      {
        $word .= $inputMessage[$i];
      }
      else
      {
        array_push($ngrams, $word);
        $word = '';
      }
      if (  ($i+1)==(strlen($inputMessage))  )
      {
        array_push($ngrams, $word);
        $word = '';
      }
  }
  return $ngrams;
}


#Can I, the robot, respond to the incoming query?
function tryToAnswer($postbackPayload, $msg, $responses, $tmacstatus)
{

  $greetingFlag = false;
  $helpFlag = false;
  $ignore = ["hello","hi","hey"];
  $count=0;
  $tmp="";
  if ($postbackPayload=="")
  {
      $ngram = ngramThis($msg);
      foreach ($responses as $k => $v)
      {
        foreach ($ngram as $n)
        {
          if ($n == $k)
          {
            if (in_array($n,$ignore))
            {
              $greetingFlag = true;
            }
            else if ($n=='help')
            {
              $helpFlag=true;
            }
            else
            {
              $tmp=$n;
              ++$count;
            }
          }
        }
      }

      if ($greetingFlag==true && $count==0)
      {
        $count=1;
        $tmp="hello";
      }
      if ($helpFlag==true && $count==0)
      {
        $count=1;
        $tmp="help";
      }
      if ($count > 1 || $count == 0)
      {
        return "message";
      }
      else
      {

        return setTheMood($tmp,$tmacstatus);
      }
  }
  else
  {
    return setTheMood($postbackPayload,$tmacstatus);
  }

}


function constructAResponse($tag,$recipientId,$msgData,$count,$Hook)
{

  switch($tag)
  {
    case "Q":
      $constructedMessage = ["text"=>$msgData[0][$count],"quick_replies"=>$msgData[1]];
      break;
    case "B":
      $constructedMessage = $msgData[2];
      break;
    case "I":
      $constructedMessage = ["attachment"=>["type"=>"image","payload"=>["url"=>"https://tinyurl.com/".$msgData[3][$Hook],"is_reusable"=>"true"]]];
      break;
    default:

      $constructedMessage = ["text"=>$msgData[0][$count]];
  }

$x = ["recipient"=>["id"=>$recipientId],"message"=>$constructedMessage];
return json_encode($x, JSON_UNESCAPED_SLASHES);
}


#Send the response
function respond($id,$responsedata,$Url,$hook)
{

  $ch = curl_init($Url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  if(!empty($responsedata[2]))
  {

    $temp=constructAResponse("B",$id,$responsedata,0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $temp);
    $result = curl_exec($ch);
  }

  for ($i=0;$i<count($responsedata[0]);++$i)
  {


    if (!empty($responsedata[3]) && $i==0)
    {
      $temp=constructAResponse("I",$id,$responsedata,$i,$hook);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $temp);
      $result = curl_exec($ch);
    }
    if (($i+1)==(count($responsedata[0])) && (!empty($responsedata[1])))
    {
      $temp=constructAResponse("Q",$id,$responsedata,$i);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $temp);
      $result = curl_exec($ch);
    }
    else
    {
      $temp=constructAResponse("",$id,$responsedata,$i);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $temp);
      $result = curl_exec($ch);
    }
    usleep(1500000);
  }



  curl_close($ch);
}
