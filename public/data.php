<?php
### Pulled data ###
$fb       = file_get_contents("php://input");
$fb       = json_decode($fb);
$message  = strtolower($fb->entry[0]->messaging[0]->message->text);
$postback = strtolower($fb->entry[0]->messaging[0]->postback->payload);
$location =  strtolower($fb->entry[0]->messaging[0]->message->attachments[0]->type);

if (empty($postback))
{
  if (empty($location))
  {
    $postback = strtolower($fb->entry[0]->messaging[0]->message->quick_reply->payload);
  }
  else
  {
    $postback = $location;
  }
}
$rid      = $fb->entry[0]->messaging[0]->sender->id;
$tmac     = file_get_contents("http://remote.tablemountain.net/output/json");
$tmac     = json_decode($tmac, true);
if ($message == 'hours' || $postback == 'hours')
{
  $date = dateChecker();
}

### Static data ###
$standardMenu =
[
  ["content_type"=>"text","title"=>"Quick tips","payload"=>"qt"],
  ["content_type"=>"text","title"=>"SOS","payload"=>"sos"],
  ["content_type"=>"text","title"=>"Tickets","payload"=>"ticket"],
  ["content_type"=>"text","title"=>"FAQ","payload"=>"faq"],
  ["content_type"=>"text","title"=>"Cableway Status","payload"=>"status"],
  ["content_type"=>"text","title"=>"Weather","payload"=>"weather"]

];

#Wind direction Conversion
$windDirections = ['SE' => 'southeasterly', 'NE' => 'northeasterly', 'SW' => 'southwesterly', 'NW' => 'northwesterly',
                   'SSE' => 'south-southeasterly', 'NNE' => 'north-northeasterly', 'SSW' => 'south-southwesterly', 'NNW' => 'north-northwesterly',
                   'ESE' => 'south-southeasterly', 'ENE' => 'north-northeasterly', 'WSW' => 'south-southwesterly', 'WNW' => 'west-northwesterly',
                    'N' => 'northerly', 'S' => 'southerly', 'E' => 'easterly', 'W' => 'westerly'];

#Operating Hours
$operationalHours = ['Jan' => ['8:00am', '6:00pm', '7:00pm'], 'Feb' => ['8:00am', '6:00pm', '7:00pm'], 'Mar' => ['8:00am', '6:00pm', '7:00pm'], 'Apr' => ['8:00am', '6:00pm', '7:00pm'],
                          'May' => ['8:30am', '5:00pm', '6:00pm'], 'Jun' => ['8:30am', '5:00pm', '6:00pm'], 'Jul' => ['8:30am', '5:00pm', '6:00pm'], 'Aug' => ['8:30am', '5:00pm', '6:00pm'],
                          'Sep' => ['8:30am', '6:00pm', '7:00pm'], 'Oct' => ['8:30am', '6:00pm', '7:00pm'], 'Nov' => ['8:30am', '7:30pm', '8:30pm'], 'Dec' => ['8:00am', '7:30pm', '8:30pm'],
                          '16'  => ['8:00am', '8:30pm', '9:30pm']];
#Responses
$responseMessages = [
    'yes' =>
    [
      [
        "I'm so very glad I could help! :D",
        "Perhaps there's something else you need? :) ..."
      ],
      $standardMenu,
    ],
    'qt' =>
    [
      [
        "Need help with anything else?"
      ],
      $standardMenu,
      [
        "attachment"=>
        [
          "type"=>"template",
          "payload"=>
          [
            "template_type"=>"button",
            "text"=>"This has some pretty useful information, for both first time guests and returning guests :)",
            "buttons"=>
            [
              ["type"=>"web_url","url"=>"http://www.tablemountain.net/content/page/quick-tips","title"=>"Quick tips"]
            ]
          ]
        ]
      ],
      ["qt"=>"y7mwn2er"]
    ],
    'no' =>
    [
      [
        "Im terribly sorry. Let's try again :)"
      ],
      $standardMenu,
    ],
    'yesplease' =>
    [
      [
        "No problem. I'll let one of them know :).",
        "Perhaps there's something else you need? :) ..."
      ],
      $standardMenu,
    ],
    'location' =>
    [
      [
        "Emergency services will be mobilised to your location right away."
      ],
      [],
      [],
      []
    ],
    'faq' =>
    [
      [
        "Here are some of our more frequently asked questions..."
      ],
      [
        ["content_type"=>"text","title"=>"Ticket price?","payload"=>"tickets"],
        ["content_type"=>"text","title"=>"Call us now","payload"=>"contact"],
        ["content_type"=>"text","title"=>"Visibility?","payload"=>"vis"],
        ["content_type"=>"text","title"=>"Full menu","payload"=>"fullmenu"]
      ]
    ],
    'status' =>
    [
      [
        "What would you like to know? I can tell you if the cableway is running today, how long you may expect to wait for a cable-car, when the first and last cars will be going, or I can tell you about the weather..."
      ],
      [
        ["content_type"=>"text","title"=>"Cableway running?","payload"=>"open"],
        ["content_type"=>"text","title"=>"Weather","payload"=>"weather"],
        ["content_type"=>"text","title"=>"Wating time","payload"=>"wait"],
        ["content_type"=>"text","title"=>"first car up/last car down","payload"=>"hours"],
        ["content_type"=>"text","title"=>"Full menu","payload"=>"fullmenu"]
      ]
    ],
    'fullmenu' =>
    [
      [
        "this is everything I can help you with"
      ],
      [
        ["content_type"=>"text","title"=>"Cableway running?","payload"=>"open"],
        ["content_type"=>"text","title"=>"Ticket price?","payload"=>"tickets"],
        ["content_type"=>"text","title"=>"Book now","payload"=>"book"],
        ["content_type"=>"text","title"=>"Call us now","payload"=>"contact"],
        ["content_type"=>"text","title"=>"Weather","payload"=>"weather"],
        ["content_type"=>"text","title"=>"Wating time?","payload"=>"wait"],
        ["content_type"=>"text","title"=>"first car up/last car down?","payload"=>"hours"],
        ["content_type"=>"text","title"=>"One way ticket price?","payload"=>"oneway"],
        ["content_type"=>"text","title"=>"Return ticket price?","payload"=>"return"],
        ["content_type"=>"text","title"=>"Visibility?","payload"=>"vis"],
      ]
    ],
    'book' =>
    [
      [
        "Have I answered all your queries?"
      ],
      [
        ["content_type"=>"text","title"=>"Yup! Thanks","payload"=>"yes","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_smiling_face_with_smiling_eyes.png"],
        ["content_type"=>"text","title"=>"Nop! Main menu","payload"=>"no","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_neutral_face.png"]
      ],
      [
        "attachment"=>
        [
          "type"=>"template",
          "payload"=>
          [
            "template_type"=>"button",
            "text"=>"This should help :)",
            "buttons"=>
            [
              ["type"=>"web_url","url"=>"http://www.webtickets.co.za/event.aspx?itemid=681080","title"=>"Book tickets"]
            ]
          ]
        ]
      ],
      []
    ],
    'wait' =>
    [
      [
        "Right now you can expect to wait " . ($tmac[0]['estimated_waiting_time_top']) . ' at the top, and ' . ($tmac[0]['estimated_waiting_time']) . ' at the bottom.'
      ],
      $standardMenu,
    ],
    'hours' =>
    [
      [
        "Weather permitting, the first car up departs at " . $operationalHours[$date][0] . ". The last car going up today is leaving at " . $operationalHours[$date][1] . " and the last car down will leave the top at " . $operationalHours[$date][2]
      ],
      $standardMenu,
    ],
    'vis' =>
    [
      [
        "Visibility from the top today is ".(strtolower($tmac[0]['visibility']))."."
      ],
      $standardMenu,
    ],
    'oneway' =>
    [
      [
        "One way fares for adults are R135, and R65 for children 4-17 years old.",
        "On Fridays, SA senior citizens pay R53 for a one way fare at our ticket office, and students pay just R70.",
        "Have I answered all your queries? :)"
      ],
      [
        ["content_type"=>"text","title"=>"Yup! Thanks","payload"=>"yes","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_smiling_face_with_smiling_eyes.png"],
        ["content_type"=>"text","title"=>"Nop! Main menu","payload"=>"no","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_neutral_face.png"]
      ]
    ],
    'return' =>
    [
      [
        "Return fares for adults are R255, and R125 for children 4-17 years old.",
        "On Fridays, SA senior citizens pay R100 for a return ticket at our ticket office, and students pay just R130.",
        "Have I answered all your queries? :)"
      ],
      [
        ["content_type"=>"text","title"=>"Yup! Thanks","payload"=>"yes","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_smiling_face_with_smiling_eyes.png"],
        ["content_type"=>"text","title"=>"Nop! Main menu","payload"=>"no","image_url"=>"https://www.hey.fr/tools/emoji/ios_emoji_neutral_face.png"]
      ]
    ],
    'message' =>
    [
      [
        'You\'re too clever for a bot like me. Would you like one of my more capable human colleagues to get to you? You can also call us on +27(0)21 424 8181.'
      ],
      [
        ["content_type"=>"text","title"=>"Yes please","payload"=>"yesplease","image_url"=>"https://tinyurl.com/ycbeh6q9"],
        ["content_type"=>"text","title"=>"Main menu","payload"=>"no","image_url"=>"https://tinyurl.com/y9r3n2uy"]
      ],
      [
        "attachment"=>
        [
          "type"=>"template",
          "payload"=>
          [
            "template_type"=>"button",
            "text"=>"We can be reached on +27(0)21 424 8181",
            "buttons"=>
            [
              ["type"=>"phone_number","payload"=>"+27214248181","title"=>"Call us now"]
            ]
          ]
        ]
      ]
    ],
    'help' =>
    [
      [
        "What can I help you with? :)",
        "Please select below"
      ],
      $standardMenu
    ],
    'whew' =>
    [
      [
        "Whew. I'm glad you're okay. What can I help you with today? :)"
      ],
      $standardMenu,
    ],
    'sos' =>
    [
      [
        "Are you a hiker in distress? Send us your location. Emergency Services will be contacted immediately."
      ],
      [
        ["content_type"=>"location"],
        ["content_type"=>"text","title"=>"Main Menu","payload"=>"whew"]
      ]
    ]
];

#Multiple keys
$Keys=
[
  [
    'hello',
    'hi',
    'hey'
  ],
  [
    'reach',
    'contact'
  ],
  [
    'closed',
    'open'
  ],
  [
    'price',
    'cost',
    'ticket',
    'tickets'
  ],
  [
    "weather",
    "raining",
    "windy",
    "sun",
    "shine",
    "warm"
  ]

];

$Value=
[
  [
    [
      "Hi there :)!. The Table Mountain Bot is here to assist.",
      "Please note: we are closed for annual maintenance from 24 July to 6 August.",
      "type the word \"help\" at any time to get a full list of commands. :)"
    ],
    $standardMenu,
    [],
    []
  ],
  [
    [
      "Need help with anything else?"
    ],
    $standardMenu,
    [
      "attachment"=>
      [
        "type"=>"template",
        "payload"=>
        [
          "template_type"=>"button",
          "text"=>"We can be reached on +27(0)21 424 8181",
          "buttons"=>
          [
            ["type"=>"phone_number","payload"=>"+27214248181","title"=>"Call us now"]
          ]
        ]
      ]
    ],
    []
  ],
  [
    [
      "The cableway is currently ".(strtolower($tmac[0]['cableway_status'])).".",
      "(last updated: ".($tmac[0]['title']).")"
    ],
    $standardMenu,
    [],
    ["open"=>"yc43zdol","closed"=>"j4wh364"]
  ],
  [
    [
      "Are you looking for one-way tickets or return tickets?"
    ],
    [
      ["content_type"=>"text","title"=>"One-way","payload"=>"oneway"],
      ["content_type"=>"text","title"=>"Return","payload"=>"return"],
      ["content_type"=>"text","title"=>"Book now!","payload"=>"book"],
    ]
  ],
  [
    [
      "The temperature is currently " . (strtolower($tmac[0]['temperature'])) . ". There's a " . (strtolower($tmac[0]['wind_strength'])) . " wind blowing from a " . $windDirections[($tmac[0]['wind_direction'])] . " direction.",
      " (last updated: ".($tmac[0]['title']).")"
    ],
    $standardMenu,
    [],
    [
      "warm"=>"y77weg9a",
      "cold"=>"yd67mxmp"
    ],
  ]
];
#Add to array
for($i=0;$i<count($Keys);++$i)
{
  $temparray=array_fill_keys($Keys[$i],$Value[$i]);
  $responseMessages = array_merge($responseMessages,$temparray);
}
